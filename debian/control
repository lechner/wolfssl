Source: wolfssl
Priority: optional
Maintainer: Felix Lechner <felix.lechner@lease-up.com>
Build-Depends:
 debhelper-compat (= 13),
 libpcap0.8-dev,
 openssl
Standards-Version: 4.6.0
Rules-Requires-Root: no
Section: libs
Vcs-Git: https://salsa.debian.org/lechner/wolfssl.git -b debian/master
Vcs-Browser: https://salsa.debian.org/lechner/wolfssl
Homepage: https://www.wolfssl.com/products/wolfssl/

Package: libwolfssl30
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: wolfSSL encryption library
 wolfSSL is a small, portable, embedded SSL/TLS programming library
 targeted for use by embedded systems developers. It was formerly
 known as CyaSSL.
 .
 It is an open source, dual licensed implementation of SSL/TLS
 (SSL 3.0, TLS 1.0, 1.1, 1.2, 1.3, DTLS 1.0 and 1.2) written in the C
 language.  wolfSSL includes both a client and server implementation,
 progressive cipher support, key and certificate generation, OCSP
 and CRL, access to the underlying cryptography modules, and more.
 .
 wolfSSL also includes an OpenSSL compatibility interface with the
 most commonly used OpenSSL functions.

Package: libwolfssl-dev
Section: libdevel
Architecture: any
Multi-Arch: no
Depends:
 libwolfssl30 (= ${binary:Version}),
 ${misc:Depends}
Description: Development files for the wolfSSL encryption library
 wolfSSL is a small, portable, embedded SSL/TLS programming library
 targeted for use by embedded systems developers. It was formerly
 known as CyaSSL.
 .
 It is an open source, dual licensed implementation of SSL/TLS
 (SSL 3.0, TLS 1.0, 1.1, 1.2, 1.3, DTLS 1.0 and 1.2) written in the C
 language.  wolfSSL includes both a client and server implementation,
 progressive cipher support, key and certificate generation, OCSP
 and CRL, access to the underlying cryptography modules, and more.
 .
 wolfSSL also includes an OpenSSL compatibility interface with the
 most commonly used OpenSSL functions.
 .
 This package contains the development files.
